Fork of https://github.com/olorin37/hbs-cli 

Simple Handlebars CLI
=====================

Simple, but already useful CLI for generating text from handlebars
templates, by feeding them with data from file (YAML parser used is
for it, so JSON is also supported), writen in Rust programming language
(with usage serde_yaml and handlebars crates).

Goal
----
The idea is to prepare utility which should be easly used from
shell script.

Usage
-----

The simplest call:

```bash
hbs-cli <template file> <properties files> > <output file>
```
or 

```bash
hbs-cli <template file> <properties files> -o <output file>
```

Or with template registration for using them as partials:

```bash
hbs-cli <template file> <properties files> -r 'partials/**/*.hbs'
```

Where `<properties files>` can be ether YAML or JSON (as YAML parser is a
JSON parser too) and `<template file>` is handlebars template. Template
is generated on standard output.

To see more options call `hbs-cli --help`.

Building
--------

```bash
# to install rust toolchain, skip if already installed
rustup toolchain install stable 

cargo build --release
```

License [![badge][license-mit-badge]](LICENSE-MIT)
-------

This software is distributed under MIT license. See `LICENSE` file in the root of the repository.

[license-mit-badge]: https://img.shields.io/badge/license-MIT-blue.svg
